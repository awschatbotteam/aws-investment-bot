var HookLambda = require('src/fundHooks');
var ReturnOBJ = require('src/returnObjects');
var IntentLambda = require('src/intentFulfillment');
var userDetailLambda = require('src/userDetail');
var mailingService = require('src/mailSender.js');
var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB.DocumentClient({"region": "us-east-1"});
var parseXlsx = require('node-xlsx');
var returnObj, excelDataObj, hook, intent, userDataObj, userData, mailit;

module.exports.taxHelper = (event, context, callback) => {
    if(!returnObj){
        returnObj = new ReturnOBJ.returnObjects();
    }
      
    if(!userDataObj){
        userDataObj = new userDetailLambda.userDetail(dynamodb);   
    }

    userDataObj.getUserData(event, function(){
        var i, intentName;
        if(!excelDataObj){
            console.log('loading xlsx');
            excelDataObj = {};
            var xlsxData = parseXlsx.parse('content/content.xlsx'),
            data = xlsxData[0].data,
            len = data.length;
            for (i = 0; i < len; i++)
              excelDataObj[data[i][0]] = data[i][1];
        }

        if(!mailit){
            mailit = new mailingService.mailSender(event, context);
        }
            
        if(!hook){
            hook = new HookLambda.fundHooks(returnObj, dynamodb, excelDataObj, userDataObj, mailit);
        }

        if(!intent){
            intent = new IntentLambda.intentFulfillment(returnObj, excelDataObj);
        }
          
        console.log(event.sessionAttributes);

        intentName = event.currentIntent.name;
        if(intentName == 'EndUserChat' || intentName == 'GreetUser' || intentName == "ProvideInformationToUser"){   
            callback(null, intent.fullfillIntent(event));
        }else{
            hook.initHook(event, callback);
        }
    });
		
}



