
module.exports.topFunds = function(returnObj, dynamodb){
    var self = this;
    var idArr = [];

    this.showTopFunds = function(event, callback, message){
        var params = self.getQueryParams(event.currentIntent.slots.riskGrade);
        self.queryDB(event, params, callback, message);
    };

    this.queryDB =  function(event, params, callback, message){
        dynamodb.query(params, function(err, data){
            response = self.getResponseText(data.Items, message);
            var responseObject = returnObj.elicitSlot(event, 'fundID', response, 
                returnObj.responseCard("Fund ID","Please choose a relevant option to view further details.", self.fundIDResponseCard()));
            if(err){
                callback(null, err);
            }else{
                callback(null, responseObject);
            }
        });
    };

    this.getQueryParams = function(riskValue){
        var params = {
            TableName: 'TaxFund',
            IndexName: "RiskGrade-3Y-index",
            KeyConditionExpression:"#risk = :riskGrade",
            ExpressionAttributeNames: {
                "#risk":"RiskGrade"
            },
            ExpressionAttributeValues: {
                ":riskGrade": riskValue
            },
            Limit: 5,
            ScanIndexForward: false
        };

        return params;
    };

    this.getResponseText = function(data, response){
        if(!response)
          var response = '*TOP FIVE FUNDS* \n';

        var val;
        idArr = [];
        for(var i in data){
            var item = data[i];
            idArr.push(item.ID);
            val = '*ID:* '+item.ID+' | *Name:* '+item.Name+' | *LaunchDate:* '+item.LaunchDate+' | *AUM:* '+item["AUM"]+' | *1Y Return:* '+item["1Y"]+' | *3Y Return:* '+item["3Y"]+'\n';
            response += val;
        }
        response += "\n This data is as per performance trend of the funds during the past three years. \n";

        return returnObj.buildMessage(response);
    };

    this.fundIDResponseCard = function(){
        return [
            {
                "text": idArr[0],
                "value": Number(idArr[0])
            },
            {
                "text": idArr[1],
                "value": Number(idArr[1])
            },
            {
                "text": idArr[2],
                "value": Number(idArr[2])
            },
            {
                "text": idArr[3],
                "value": Number(idArr[3])
            },
            {
                "text": idArr[4],
                "value": Number(idArr[4])
            }
        ]
    };

}

