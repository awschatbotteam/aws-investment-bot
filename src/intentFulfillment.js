
module.exports.intentFulfillment = function(returnObj, excelDataObj){    
    var fundName;

    this.fullfillIntent = function(event){
        var intentName = event.currentIntent.name;
        var message, userName, userAge;
        var self = this;
        if(intentName == "GreetUser"){
            userName = event.sessionAttributes.userName;
            userAge = event.sessionAttributes.userAge;
            if(!userAge){
                message =  'Hi, '+ userName +'! I am your personal tax saver recommender. I analyze your financial preferences to help you choose an appropriate option to reduce your tax liability.'
            }else{
                message =  'Hi, '+ userName +'! Welcome back, it is great to see you again.'
            }
            return returnObj.close(event.sessionAttributes,
                'Fulfilled',
                returnObj.buildMessage(message),
                returnObj.responseCard("Schemes Available Under Section 80C","Please choose a relevant option.", self.greetingResponseCard()));

        }else if(intentName == "EndUserChat"){
            message =  'Thanks for chatting with us, Happy Investing!'
            return returnObj.close(event.sessionAttributes,
                'Fulfilled',
                returnObj.buildMessage(message));

        }else if(intentName == "ProvideInformationToUser"){
            console.log(event);
            var $instrument = event.currentIntent.slots.instrument;
            if($instrument && $instrument != 'it'){
                fundName = event.currentIntent.slots.instrument.toLowerCase();
            }
            var intentType, 
            fullForm = event.currentIntent.slots.fullForm,
            procedure = event.currentIntent.slots.procedure,
            keyFeatures = event.currentIntent.slots.keyFeatures,
            advantages = event.currentIntent.slots.advantages,
            risk = event.currentIntent.slots.risk,
            taxBenefits = event.currentIntent.slots.taxBenefits,
            lockIn = event.currentIntent.slots.lockIn,
            minimum = event.currentIntent.slots.minimum,
            maximum = event.currentIntent.slots.maximum,
            tenure = event.currentIntent.slots.tenure,
            eligibility = event.currentIntent.slots.eligibility,
            rate = event.currentIntent.slots.rate;

            if(fullForm && fullForm != null){
                intentType = 'fullForm';
            }else if(procedure && procedure != null){
                intentType = 'procedure';
            }else if(keyFeatures && keyFeatures != null){
                intentType = 'keyFeatures';
            }else if(advantages && advantages != null){
                intentType = 'advantages';
            }else if(risk && risk != null){
                intentType = 'risk';
            }else if(taxBenefits && taxBenefits != null){
                intentType = 'taxBenefits';
            }else if(lockIn && lockIn != null){
                intentType = 'lockIn';
            }else if(minimum && minimum != null){
                intentType = 'minimum';
            }else if(maximum && maximum != null){
                intentType = 'maximum';
            }else if(tenure && tenure != null){
                intentType = 'tenure';
            }else if(eligibility && eligibility != null){
                intentType = 'eligibility';
            }else if(rate && rate != null){
                intentType = 'rate';
            }

            message =  self.getWikiFulfillment(fundName,intentType);

            return returnObj.close(event.sessionAttributes, 'Fulfilled', returnObj.buildMessage(message));
        }
        
    };

    this.getWikiFulfillment = function(fundName, intentType){
        var message, key;

        if(fundName && fundName != null && intentType && intentType != null){
            key = fundName+'_'+intentType;            
            message = excelDataObj[key];
        }else if(fundName && fundName != null){
            key = fundName+'_meaning';            
            message = excelDataObj[key];
        }else{
            message = "Sorry, I did not have this information at the moment.";
        }

        return message;
    };

    this.greetingResponseCard = function(){
        return [
            {
                "text": "ELSS",
                "value": "want to invest in ELSS"
            },
            {
                "text": "NSC",
                "value": "want to invest in NSC"
            },
            {
                "text": "PPF",
                "value": "want to invest in PPF"
            },
            {
                "text": "Tax Saving FD",
                "value": "want to invest in tsfd"
            },
            {
                "text": "Help Me!",
                "value": "help me choose"
            }
        ]
    };
    
}