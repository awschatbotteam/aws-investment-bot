var request = require("request");
var slackToken = "xoxp-198105619778-198850505893-210825354183-c6f708d00e08f02d33bb3431b5923172";

module.exports.userDetail = function(dynamodb){
	var table = "UserDetail";
	var params;
	
	this.getUserData = function(event, callback){
		var idArr = event.userId.split(':');
		var slackUserId = idArr[(idArr.length - 1)];
		var userid = idArr[(idArr.length - 2)]+':'+ slackUserId;
		var self = this;
		
		console.log(userid);

		params = {
		    TableName: table,
		    Key:{
		        "userId": userid
		    }
		};

		dynamodb.get(params, function(err, userData) {
	    if (err) {
        console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	    	console.log("GetItem succeeded:", userData.Item);
	    	if(userData.Item && userData.Item.userId){
		    	event.sessionAttributes = {
		      	'userId': userData.Item.userId,
	          'userName': userData.Item.name,
	          'userAge': userData.Item.age,
	          'userEmail': userData.Item.email,
	          'userPhone': userData.Item.phoneNo,
	          'fundType': userData.Item.fundType
		       };
	       callback();
	    	}else{
	    		self.getSlackUserName(slackUserId, userid, event, callback);
	    	}
	      
	    }

		});
	};

	this.getSlackUserName = function(slackUserId, userid, event, callback){
		var url = "https://slack.com/api/users.info?token="+slackToken+"&user="+slackUserId+"&pretty=1";
		var name, slackData;
		console.log('trying to get user data for user: '+ userid);

		request(url, function(error, response, body) {
			slackData = JSON.parse(body);
		  isOk = slackData.ok;
		  if(isOk || isOk == 'true'){
		  	name = slackData.user.profile.first_name;
			  if(!name || name == ''){
			  	name = slackData.user.name;
			  }  
		  }else{
		  	name = 'there';
		  }

		  console.log('slack user name is : '+ name);
			event.sessionAttributes = {
				'userId': userid,
	      'userName': name,
	      'fundType': ''
	    };
		  callback(); 	
		});

		
		
	}

	this.updateUserData = function(id, fundType, slots){
		var risk = slots.riskGrade || 'Low';
		params = {
		    TableName:table,
		    Key:{
		        "userId": id
		    },
		    UpdateExpression: "set riskAppetite = :r, fundType=:f, amount=:a",
		    ExpressionAttributeValues:{
		        ":r":risk,
		        ":f":fundType,
		        ":a":slots.corpus
		    },
		    ReturnValues:"UPDATED_NEW"
		};
		dynamodb.update(params, function(err, data) {
	    if (err) {
	        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
	    }
		});
	};

	this.updateUserContanctDetails = function(id, slots){
		params = {
		    TableName:table,
		    Key:{
		        "userId": id
		    },
		    UpdateExpression: "set email = :e, phoneNo=:p",
		    ExpressionAttributeValues:{
		        ":e":slots.email,
		        ":p":slots.phone
		    },
		    ReturnValues:"UPDATED_NEW"
		};
		dynamodb.update(params, function(err, data) {
	    if (err) {
	        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
	    }
		});
	};

	this.putUserData = function(id, name, fundType, slots){
		params = {
		    TableName:table,
		    Item:{
		    		"userId": id,
		        "name": name,
		        "age": slots.userAge,
		        "amount": slots.corpus,
		        "fund": 'fund',
		        "fundType": fundType,
		        "riskAppetite": slots.riskGrade,
		        "email": slots.email,
		        "phoneNo": slots.phone
		    }
		};
		dynamodb.put(params, function(err, data) {
	    if (err) {
	        console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("Added Item succeeded:", JSON.stringify(data, null, 2));
	    }
		});
	};
	
}