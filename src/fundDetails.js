module.exports.fundDetails = function(returnObj, dynamodb){
    var self = this;
    var idArr;

    this.showFundDetail = function(event, callback){
        var params = self.getDetailQueryParams(event.currentIntent.slots.fundID);
        self.queryDB(event, params, callback);
    }

    this.queryDB =  function(event, params, callback){
        dynamodb.query(params, function(err, data){
            var response = self.getResponseText(data.Items, event.currentIntent.slots.payMode);
            var responseObject = returnObj.confirmIntent(event, response);
            if(err){
                callback(null, err);
            }else{
                callback(null, responseObject);
            }
        });
    };

    this.getDetailQueryParams = function(idValue){
        var params = {
            TableName: 'TaxFund',
            KeyConditionExpression:"#id = :fundID",
            ExpressionAttributeNames: {
                "#id":"ID"
            },
            ExpressionAttributeValues: {
                ":fundID": idValue
            },
            Limit: 1,
            ScanIndexForward: false
        };

        return params;
    };

    this.getResponseText = function(data, paymode){
        var item = data[0];
        var response = '*Basic Information* \n';
        response += ' *Fund Name:* '+item.Name+'\n'
        response += ' *Launch Date:* '+item.LaunchDate+'\n';
        response += ' *Category:* '+item.Category+'\n';
        response += ' *AUM:* '+item.AUM+'\n';
        response += ' *Benchmark:* '+item.Benchmark+'\n';
        response += ' *Return Grade:* '+item.ReturnGrade+'\n';
        response += ' *Risk Grade:* '+item.RiskGrade+'\n\n';
        response += '*'+ paymode +' Returns* \n';
        response += ' *Since Inception:* '+item["SI"]+'\n';
        response += ' *3 months Returns:* '+item["3M"]+'\n';
        response += ' *1 year Returns:* '+item["1Y"]+'\n';
        response += ' *3 year Returns:* '+item["3Y"]+'\n';
        response += ' *5 year Returns:* '+item["5Y"];
        
        response += "\n \n Would you like us to contact you for more assistance?";

        return returnObj.buildMessage(response);
    };

}

