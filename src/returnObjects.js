module.exports.returnObjects = function() {
	
	this.elicitSlot = function(event, slotToElicit, message, responseCard) {
		return {
			"sessionAttributes": event.sessionAttributes,
			"dialogAction": {
				"type": 'ElicitSlot',
				"intentName": event.currentIntent.name,
				"slots": event.currentIntent.slots,
				"slotToElicit": slotToElicit,
				"message": message,
				"responseCard": responseCard
			}
		};
	};

	this.confirmIntent = function(event, message, responseCard) {
		return {
			"sessionAttributes": event.sessionAttributes,
			"dialogAction": {
				"type": 'ConfirmIntent',
				"intentName": event.currentIntent.name,
				"slots": event.currentIntent.slots,
				"message": message,
				"responseCard": responseCard
			}
		};
	};

	this.close = function(sessionAttributes, fulfillmentState, message, responseCard) {
		return {
			"sessionAttributes": sessionAttributes,
			"dialogAction": {
				"type": 'Close',
				"fulfillmentState": fulfillmentState,
				"message": message,
				"responseCard": responseCard
			}
		};
	};

	this.delegate = function(sessionAttributes, slots) {
		return {
			"sessionAttributes": sessionAttributes,
			"dialogAction": {
				"type": 'Delegate',
				"slots": slots,
			}
		};
	};

	this.buildMessage = function(messageContent) {
		return {
			"contentType": 'PlainText',
			"content": messageContent
		};
	};

	this.responseCard = function(title, subTitle, buttons){
		return {
      "version": 1,
      "contentType": "application/vnd.amazonaws.card.generic",
      "genericAttachments": [
          {
             "title": title,
             "subTitle": subTitle,
             "buttons":buttons
           } 
       ] 
     }
	};
}

