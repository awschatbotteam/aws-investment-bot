var topFundsLambda = require('src/topFunds');
var detailFundLambda = require('src/fundDetails');

module.exports.fundHooks = function(returnObj, dynamodb, excelDataObj, userDataObj, mailSender){
    var funds = new topFundsLambda.topFunds(returnObj, dynamodb);
    var fundDetail = new detailFundLambda.fundDetails(returnObj, dynamodb);
    var instrumentName, self = this;

    this.initHook = function(intentRequest, callback){
        var $intentName = intentRequest.currentIntent.name;
        var $age = intentRequest.currentIntent.slots.userAge;
        var $corpus = Number(intentRequest.currentIntent.slots.corpus);
        var $sessionAttributes = intentRequest.sessionAttributes;

        if(!$age && intentRequest.sessionAttributes.userAge){
            intentRequest.currentIntent.slots.userAge = intentRequest.sessionAttributes.userAge;
            callback(null, returnObj.elicitSlot(intentRequest, 
                'corpus', 
                returnObj.buildMessage("How much money would you like to invest annually?")));
        }else if($age && ($age > 60 || $age < 18)){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'userAge', 
                returnObj.buildMessage("Investment age should be between 18 and 60 years.")));
        }else if($corpus && $intentName != 'HighRiskInstrumentRecommender' && $corpus < 100){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'corpus', 
                returnObj.buildMessage("Investment amount should be a minimum of 100 INR.")));
        }else if($corpus && $intentName == 'HighRiskInstrumentRecommender' && ($corpus < 500 || $corpus%10 != 0)){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'corpus', 
                returnObj.buildMessage("Investment amount should be a minimum of 500 INR and a multiple of 10.")));
        }else if($age && $corpus && $intentName == 'LowRiskInstrumentRecommender'){
            self.fulfillHook(intentRequest, callback, $intentName);
        }else if($age && $corpus){
            self.instrumentHook(intentRequest, callback, $intentName);
        }else{
            callback(null, returnObj.delegate($sessionAttributes, 
                intentRequest.currentIntent.slots));
        }

    };

    this.instrumentHook = function(intentRequest, callback, intentName){
        var $duration = intentRequest.currentIntent.slots.duration;
        var $payment = intentRequest.currentIntent.slots.payMode;
        var $sessionAttributes = intentRequest.sessionAttributes;
        var $risk;

        if(intentName == 'HighRiskInstrumentRecommender'){
            $risk = intentRequest.currentIntent.slots.riskGrade;
        }else{
            $risk = intentRequest.currentIntent.slots.riskFluctuation;
        }

        if(!$payment){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'payMode',
                returnObj.buildMessage("How would you like to invest this money?"),
                returnObj.responseCard("Payment Mode","Please choose a relevant option.", self.paymentModeResponseCard())));
        }else if($duration && $duration < 3){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'duration', 
                returnObj.buildMessage("Investment duration should be more than 3 years.")));
        }else if($duration && intentName == 'HighRiskInstrumentRecommender' && !$risk){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'riskGrade',
                returnObj.buildMessage("What's your risk appetite?"),
                returnObj.responseCard("Risk Grade","Please choose a relevant option.", self.highRiskGradeResponseCard())));
        }else if($duration && intentName == 'InstrumentRecommender' && !$risk){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'riskFluctuation',
                returnObj.buildMessage("Which of the following best describes the amount of risk you can tolerate in your investment?"),
                returnObj.responseCard("Risk appetite","Please choose a relevant option.", self.instrumentRiskGradeResponseCard())));
        }else if(intentName == 'HighRiskInstrumentRecommender' && $payment && $duration && $risk){
            self.highRiskHook(intentRequest, callback);
        }else if(intentName == 'InstrumentRecommender' && $payment && $duration && $risk){
            self.recommendInstrumentHook(intentRequest, callback);
        }else{
            callback(null, returnObj.delegate($sessionAttributes, 
                intentRequest.currentIntent.slots));
        }

    }

    this.highRiskHook = function(intentRequest, callback){
        var $fundID = intentRequest.currentIntent.slots.fundID;

        if(!$fundID){
            funds.showTopFunds(intentRequest, callback);
        }else{
           self.fulfillHook(intentRequest, callback, 'HighRiskInstrumentRecommender'); 
        }
    };

    this.recommendInstrumentHook = function(intentRequest, callback){
        var $duration = intentRequest.currentIntent.slots.duration;
        var $risk = intentRequest.currentIntent.slots.riskFluctuation;
        var $fundID = intentRequest.currentIntent.slots.fundID;
        var $instrument = intentRequest.currentIntent.slots.lowRiskInstrument;
        var $message = 'We recommend that you invest in ELSS Funds. \n\n *What is ELSS?* \n'+ excelDataObj['elss_meaning'] + '\n \n';
        console.log($message);
        if($risk == 'Aggressive' && !$fundID && !$instrument){
            intentRequest.currentIntent.slots.riskGrade = 'High';
            $message += 'Here are the top five *High* risk ELSS Funds. \n';
            funds.showTopFunds(intentRequest, callback, $message);
        }else if($duration < 5 && $risk == 'Moderate' && !$fundID && !$instrument){
            intentRequest.currentIntent.slots.riskGrade = 'Average';
            $message += 'Here are the top five *Moderate* risk ELSS Funds. \n';
            funds.showTopFunds(intentRequest, callback, $message);
        }else if($duration < 5 && $risk == 'Safety Oriented' && !$fundID && !$instrument){
            intentRequest.currentIntent.slots.riskGrade = 'Low';
            $message += 'Here are the top five *Low* Risk ELSS Funds \n';
            funds.showTopFunds(intentRequest, callback, $message);
        }else if($duration >= 5 && $risk == 'Moderate' && !$fundID && !$instrument){
            intentRequest.currentIntent.slots.riskGrade = 'Below Average';
            $message += 'Here are the top five *Below Average* risk ELSS Funds \n';
            funds.showTopFunds(intentRequest, callback, $message);
        }else if($duration >= 5 && $risk == 'Safety Oriented' && !$fundID && !$instrument){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'lowRiskInstrument',
                self.recommendInstrumentRespondMessage(),
                returnObj.responseCard("Low Risk Instruments","Please choose a relevant option.", self.instrumentResponseCard())));  
        }else if($fundID){
            self.fulfillHook(intentRequest, callback, 'HighRiskInstrumentRecommender');
        }else if($instrument){
            self.fulfillHook(intentRequest, callback, 'LowRiskInstrumentRecommender');
        }
    };

    this.fulfillHook = function(intentRequest, callback, intentName){
        console.log(intentRequest);
        var $confirm = intentRequest.currentIntent.confirmationStatus;
        var $email = intentRequest.currentIntent.slots.email;
        var $phone = intentRequest.currentIntent.slots.phone;

        if(intentName == 'LowRiskInstrumentRecommender' && $confirm == 'None' && !$email){
            var $instrument = intentRequest.currentIntent.slots.lowRiskInstrument.toLowerCase();
            var $corpus = Number(intentRequest.currentIntent.slots.corpus);
            instrumentName = $instrument;
            callback(null, returnObj.confirmIntent(intentRequest, self.LowRiskRespondMessage($corpus, $instrument)));
        }else if(intentName == 'HighRiskInstrumentRecommender' && $confirm == 'None' && !$email){
            instrumentName = 'elss';
            fundDetail.showFundDetail(intentRequest, callback);
        }else if($email && !self.velidateEmail($email, intentRequest)){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'email', 
                returnObj.buildMessage("Please enter valid email address.")));
        }else if($email && self.velidateEmail($email, intentRequest) && !$phone){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'phone', 
                returnObj.buildMessage("Please provide your contact number.")));
        }else if($phone || (intentRequest.sessionAttributes.userEmail && $confirm == 'Confirmed')){
            callback(null, returnObj.close(intentRequest.sessionAttributes,
                'Fulfilled', 
                returnObj.buildMessage("Thanks! We will get back to you, Happy Investing!")));
            self.userDataUpdate(intentRequest, true, instrumentName);
        }else if($confirm == 'Confirmed'){
            callback(null, returnObj.elicitSlot(intentRequest, 
                'email', 
                returnObj.buildMessage("Please provide your email address.")));
        }else if($confirm == 'Denied'){
            callback(null, returnObj.close(intentRequest.sessionAttributes,
                'Fulfilled', 
                returnObj.buildMessage("Thanks for chatting with us, Happy Investing!")));
            self.userDataUpdate(intentRequest, false, instrumentName);
        }

    };

    this.userDataUpdate = function(intentRequest, isConfirmed, instrument){
        var isUser = intentRequest.sessionAttributes.userAge;
        var isEmail = intentRequest.sessionAttributes.userEmail;
        var userid = intentRequest.sessionAttributes.userId;
        var userName = intentRequest.sessionAttributes.userName;
        var slots = intentRequest.currentIntent.slots;

        if(isUser){
            userDataObj.updateUserData(userid, instrument, slots);
            if(!isEmail && isConfirmed){
                userDataObj.updateUserContanctDetails(userid, slots);
            }
        }else{
            console.log(userid);
            userDataObj.putUserData(userid, userName, instrument, slots);
        }

        mailSender.sendEmail(userName, slots, instrument);
        
    }

    this.LowRiskRespondMessage = function(corpus, fundName){
        var rate, tenure, name;
        switch(fundName){
            case "ppf":
              rate = 0.079;
              tenure = 15;
              name = "PPF";
              break;
            case "tsfd":
              rate = 0.0775;
              tenure = 5;
              name = "Tax Saving FD";
              break;
            case "nsc":
              rate = 0.08;
              tenure = 5;
              name = "NSC";
              break;
        }
        var key = fundName+'_rate';
        var message = '*Interest Rate*\n'+excelDataObj[key]+'\n \n';
        message += "*Maturity Amount* \n Your corpus of *"+ corpus+ "* invested annually will reach an estimated maturity amount of *" + self.calculateInterestRate(corpus, rate, tenure) + " INR* at a rate of *"+ (rate*100) + "%* after *"+tenure+"* years in a *"+name+"* account.\n \n";
        
        key = fundName+'_procedure';
        message += '*Procedure*\n'+excelDataObj[key]+'\n \n';

        key = fundName+'_risk';
        message += '*Disclaimer*\n'+excelDataObj[key];
        message += "\n \n Would you like us to contact you for more assistance?"
        
        return returnObj.buildMessage(message);
    };

    this.recommendInstrumentRespondMessage = function(){
        var message = 'Here are the Low Risk Instruments you can invest in. \n\n',
        key = 'ppf_meaning';
        message += '*What is PPF?*\n'+excelDataObj[key]+'\n\n';
        key = 'nsc_meaning';
        message += '*What is NSC?*\n'+excelDataObj[key]+'\n\n';
        key = 'tsfd_meaning';
        message += '*What is Tax Saving FD?*\n'+excelDataObj[key]+'\n\n';

        return returnObj.buildMessage(message);
    };

    this.paymentModeResponseCard = function(){
        return [
            {
                "text": "Lump Sum",
                "value": "Lump Sum"
            },
            {
                "text": "Instalment",
                "value": "Instalments"
            }
        ]
    };

    this.highRiskGradeResponseCard = function(){
        return [
            {
                "text": "Low",
                "value": "Low"
            },
            {
                "text": "Below Average",
                "value": "Below Average"
            },
            {
                "text": "Average",
                "value": "Average"
            },
            {
                "text": "Above Average",
                "value": "Above Average"
            },
            {
                "text": "High",
                "value": "High"
            }
        ]
    };

    this.instrumentRiskGradeResponseCard = function(){
        return [
            {
                "text": "Aggressive",
                "value": "Aggressive"
            },
            {
                "text": "Moderate",
                "value": "Moderate"
            },
            {
                "text": "Safety Oriented",
                "value": "Safety Oriented"
            }
        ]
    };

    this.instrumentResponseCard = function(){
        return [
            {
                "text": "PPF",
                "value": "ppf"
            },
            {
                "text": "NSC",
                "value": "nsc"
            },
            {
                "text": "Tax Saving FD",
                "value": "tsfd"
            }
        ]
    };

    this.calculateInterestRate = function(principal, rate, tenure){
        var i, result, year, 
        yearlyAddition = Number(principal), 
        principal = Number(principal);
        for(year = 1; year <= tenure; year++){
            result = principal * (1 + rate);
            principal = result + yearlyAddition;
        }

        return Math.floor(result);

    };

    this.velidateEmail =  function(email, intentRequest){
        var $arr = email.split('|');

        if($arr.length > 1){
          intentRequest.currentIntent.slots.email = $arr[1];
          email = intentRequest.currentIntent.slots.email; 
        }
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(email);
    };

    this.velidatePhone =  function(num){
        return num.match(/\d/g).length===10;
    };
}

